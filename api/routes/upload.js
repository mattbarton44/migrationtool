var express = require('express');
var router = express.Router();
var multer = require('multer');
var xlsxtojson = require("xlsx-to-json-lc");

const dbc = require('./../controllers/db');


router.post('/', function(req, res, next) {

    
    let checker = 0;
    let parsedData = {
        works: [],
        hierarchy: [],
    }

    var storage = multer.diskStorage({ //multers disk storage settings
        destination: function (req, file, cb) {
            cb(null, './uploads/')
        },
        filename: function (req, file, cb) {
            var datetimestamp = Date.now();
            cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
        }
    });
    
    var upload = multer({ //multer settings
        storage: storage,
        fileFilter : function(req, file, callback) { //file filter
            if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length-1]) === -1) {
                return callback(new Error('Wrong extension type'));
            }
            callback(null, true);
        }
    }).single('file');
    
    const cs_works = new dbc.pgp.helpers.ColumnSet(['name', 'nexus', 'site_class', 'batch', 'size', 'type'], {table: 'works'});
    const cs_hierarchy = new dbc.pgp.helpers.ColumnSet(['works', 'process', 'function', 'asset', 'point'], {table: 'hierarchy'});
    
    let processData = function(data, type, batch, res) {
        checker++;
        for(let i=0; i<data.length; i++)
        {
            if(data[i].name != null)
            {
                let datum
                if(type == 'works')
                {
                    datum = {
                        name: data[i].name,
                        nexus: data[i].nexus,
                        site_class: data[i].site_class,
                        batch: batch,
                        size: 'Simple',
                        type: null,
                    }
                }
                else
                {
                    datum = {
                        works: data[i].worksname,
                        process: data[i].processname,
                        function: data[i].functionname,
                        asset: data[i].assetname,
                        point: data[i].name,
                    }
                }
                parsedData[type].push(datum);
            }
        }
    
        if(checker == 4) { 
            const insert_works = dbc.pgp.helpers.insert(parsedData.works, cs_works);
            dbc.db.none(insert_works)
            .then(() => {
                const insert_hierarchy = dbc.pgp.helpers.insert(parsedData.hierarchy, cs_hierarchy);
                dbc.db.none(insert_hierarchy)
                .then(() => {
                    res.json({error_code:0,err_desc:null, data: 'success'}); 
                })
                .catch(error => {
                    res.json({error_code:1,err_desc:"DB Insert Failed", data: error});
                });
            })
            .catch(error => {
                res.json({error_code:1,err_desc:"DB Insert Failed", data: error});
            });
        }
    
    }
    

    upload(req,res,function(err){

        if(err){
             res.json({error_code:1,err_desc:err});
             return;
        }
        /** Multer gives us file info in req.file object */
        if(!req.file){
            res.json({error_code:1,err_desc:"No file passed"});
            return;
        }
        /** Check the extension of the incoming file and use the appropriate module */

        try {
            xlsxtojson({ input: req.file.path, output: null, lowerCaseHeaders:true, sheet: "Sheet 1"
            }, function(err,result) {
                if(err) {  return res.json({error_code:1,err_desc:err, data: null}); }
                processData(result, 'works', req.body.batch, res)
            });
        } catch (e){ res.json({error_code:1,err_desc:"Corupted excel file"}); }

        try {
            xlsxtojson({ input: req.file.path, output: null, lowerCaseHeaders:true, sheet: "Sheet 1"
            }, function(err,result) {
                if(err) {  return res.json({error_code:1,err_desc:err, data: null}); }
                processData(result,'works',req.body.batch, res)
            });
        } catch (e){ res.json({error_code:1,err_desc:"Corupted excel file"}); }

        try {
            xlsxtojson({ input: req.file.path, output: null, lowerCaseHeaders:true, sheet: "Sheet 2"
            }, function(err,result) {
                if(err) {  return res.json({error_code:1,err_desc:err, data: null}); }
                processData(result,'hierarchy',req.body.batch, res)
            });
        } catch (e){ res.json({error_code:1,err_desc:"Corupted excel file"}); }

        try {
            xlsxtojson({ input: req.file.path, output: null, lowerCaseHeaders:true, sheet: "Sheet 2"
            }, function(err,result) {
                if(err) {  return res.json({error_code:1,err_desc:err, data: null}); }
                processData(result,'hierarchy',req.body.batch, res)
            });
        } catch (e){ res.json({error_code:1,err_desc:"Corrupted excel file"}); }




    })
   
});

module.exports = router;
