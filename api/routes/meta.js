var express = require('express');
var router = express.Router();

const dbc = require('./../controllers/db');

router.get('/', function(req, res, next) {

    dbc.db.many('SELECT * from meta')
    .then(function (data) {
        res.send(JSON.stringify(data));
    })
    .catch(function (error) {
        res.send(JSON.stringify([]));
        console.log('ERROR:', error)
  })
});


router.post('/', function(req, res, next) {
        
    let data = req.body.data;
    let site_class = req.body.site_class;
    
    dbc.db.result('delete from meta where site_class = $1', site_class)
    .then(function (result) {

        const cs = new dbc.pgp.helpers.ColumnSet(['site_class', 'process', 'function', 'asset', 'shown', 'routed', 'mandatory', 'object', 'data_id'], {table: 'meta'});
        const insert = dbc.pgp.helpers.insert(data, cs);
        dbc.db.none(insert)
        .then(() => {
            res.json({error_code:0,err_desc:null, data: 'success'}); 
        })
        .catch(error => {
            res.json({error_code:1,err_desc:"DB Insert Failed", data: error});
        });

    })
    .catch(function (err) {
      return next(err);
    });    
   
});


module.exports = router;
