var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.send('getting list of objects');
});

module.exports = router;
