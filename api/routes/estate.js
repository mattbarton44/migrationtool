var express = require('express');
var router = express.Router();

const dbc = require('./../controllers/db');

router.get('/', function(req, res, next) {

    dbc.db.many('SELECT * from estate')
    .then(function (data) {
        res.send(JSON.stringify(data));
    })
    .catch(function (error) {
        res.send('Ran in to an error: ' + JSON.stringify(error));
        console.log('ERROR:', error)
  })
});

module.exports = router;
