var express = require('express');
var router = express.Router();

const dbc = require('./../controllers/db');

router.get('/', function(req, res, next) {

    dbc.db.many('SELECT * from works')
    .then(function (data) {
        res.send(JSON.stringify(data));
    })
    .catch(function (error) {
        res.send('Ran in to an error: ' + JSON.stringify(error));
        console.log('ERROR:', error)
  })
});


router.post('/', function(req, res, next) {
        
    let data = req.body.data;
    let site_class = req.body.site_class;
    
    dbc.db.result('delete from works where site_class = $1', site_class)
    .then(function (result) {

        const cs = new dbc.pgp.helpers.ColumnSet(['name', 'nexus', 'site_class', 'batch', 'size', 'type'], {table: 'works'});
        const insert = dbc.pgp.helpers.insert(data, cs);
        dbc.db.none(insert)
        .then(() => {
            res.json({error_code:0,err_desc:null, data: 'success'}); 
        })
        .catch(error => {
            res.json({error_code:1,err_desc:"DB Insert Failed", data: error});
        });

    })
    .catch(function (err) {
      return next(err);
    });    
   
});

router.post('/delete', function(req, res, next) {
        
    let list = req.body.data;
    
    dbc.db.result('delete from works where name IN ($1:csv)', [list])
    .then(function (result) {

        dbc.db.result('delete from hierarchy where works IN ($1:csv)', [list])
        .then(function (result) {
            res.json({error_code:0,err_desc:null, data: 'success'}); 
        })
        .catch(function (err) {
            res.json({error_code:1,err_desc:"DB delete Failed", data: error});
        }); 

    })
    .catch(function (err) {
      return next(err);
    });    
   
});


module.exports = router;
