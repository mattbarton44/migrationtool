const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');

const indexRouter = require('./routes/index');
const uploadRouter = require('./routes/upload');
const worksRouter = require('./routes/works');
const estateRouter = require('./routes/estate');
const hierarchyRouter = require('./routes/hierarchy');
const objectsRouter = require('./routes/objects');
const templtesRouter = require('./routes/templates');
const metaRouter = require('./routes/meta');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors({origin: 'http://localhost:3000'}));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/upload', uploadRouter);
app.use('/works', worksRouter);
app.use('/estate', estateRouter);
app.use('/hierarchy', hierarchyRouter);
app.use('/objects', objectsRouter);
app.use('/templates', templtesRouter);
app.use('/meta', metaRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  
  // Website you wish to allow to connect
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
