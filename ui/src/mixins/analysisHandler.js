export default {

    components: {
    },
    data () {
        return {
        };
    },
    watch: {
    },
    computed: {
    },
    methods: {


        calculateTypeTemplates(activeClass, activeType) {

            let sdata = this.applyMetaConfiguration(activeClass, activeType);

            let raw_templates = {};
            for(let works in this.data.works)
            {
                let w = this.data.works[works];
                if((w.type == activeType || activeType == 'ALL') && activeClass == w.site_class)
                {
                    let hash = '';
                    let list = [];
                    for(let s=0; s<sdata.length; s++)
                    {
                        let row = sdata[s]
                        if(row.mandatory == 'Yes')
                        {
                            hash = hash+'m';
                            list.push(row)
                        }
                        else if(row.routed == 'No')
                        {
                            hash = hash+'r';
                            list.push(row)
                        }
                        else if(row.works[works] != null) // does exist
                        {
                            hash = hash+'x';
                            list.push(row)
                        }
                        else  // doesn't exist
                        {
                            hash = hash+'o';
                            list.push(null)
                        }
                    }    
                    if(raw_templates[hash] == null)
                    {
                        raw_templates[hash] = {};
                    }                
                    raw_templates[hash][works] = list;
                }
            }

            
            let templates = {};
            let id = 1;
            for(let rt in raw_templates) //each template
            {
                let new_hash = '';
                let h_data = [];
                for(let c=0; c<rt.length; c++)
                {
                    let hierarchy = sdata[c];
                    let index = rt.charAt(c);
                    let new_char = index;

                    if(index == 'r' || index == 'm')
                    {
                        let count = 0;
                        for(let wr in raw_templates[rt]) // each works in temlpate
                        {
                            let e = raw_templates[rt][wr][c];
                            if(e.works[wr] != null)
                            {
                                count++
                            }
                        }
                        if(count == 0)
                        { // doesn't exist in any of the works that use the template therefore can be ignored.
                            new_char = 'o';
                        }
                    }
                    new_hash = new_hash+new_char;
                    if(new_char != 'o')
                    {
                        h_data.push(hierarchy);
                    }
                }

                
                if(templates[new_hash] == null)
                {
                    templates[new_hash] = {
                        id: id,
                        works: {},
                        data: h_data,
                    };
                    id++;
                }
                for(let key in raw_templates[rt])
                {
                    templates[new_hash].works[key] = true;
                }

            }

            let metrics = {};
            for(let t in templates)
            {
                if(metrics[templates[t].id] == null)
                {
                    metrics[templates[t].id] = Object.keys(templates[t].works).length;
                }
            }

            return { metrics: metrics, list: templates, data: sdata } ;
        },


        applyMetaConfiguration(activeClass, activeType) {

            let lookup = {};

            for(let i=0; i<this.data.hierarchy.length; i++)
            {
                let row = this.data.hierarchy[i];

                if((activeType == this.data.works[row.works].type || activeType == 'ALL') && activeClass ==  this.data.works[row.works].site_class )
                {
                    let key = `${row.process} ${row.function} ${row.asset}`;
                    
                    if(lookup[key] == null)
                    {
                        lookup[key] = {
                            process: row.process,
                            function: row.function,
                            asset: row.asset,
                            shown: 'Yes',
                            routed: 'Yes',
                            mandatory: 'No',
                            object: null,
                            data_id: null,
                            works: {},
                        }
                    }

                    lookup[key].works[row.works] = true;

                    // check if meta exists
                    if(this.data.meta[activeClass] != null)
                    {
                        let p_key = `${row.process}`;
                        let f_key = `${row.process} ${row.function}`;
                        let a_key = `${row.process} ${row.function} ${row.asset}`;

                        if(this.data.meta[activeClass].p[p_key] != null) // if process exists that is most important
                        {
                            lookup[key].shown = this.data.meta[activeClass].p[p_key].shown;
                            lookup[key].routed = this.data.meta[activeClass].p[p_key].routed;
                            lookup[key].mandatory = this.data.meta[activeClass].p[p_key].mandatory;
                            lookup[key].object = this.data.meta[activeClass].p[p_key].object;
                            lookup[key].data_id = this.data.meta[activeClass].p[p_key].data_id;
                        }
                        else if(this.data.meta[activeClass].f[f_key] != null) // then function
                        {
                            lookup[key].shown = this.data.meta[activeClass].f[f_key].shown;
                            lookup[key].routed = this.data.meta[activeClass].f[f_key].routed;
                            lookup[key].mandatory = this.data.meta[activeClass].f[f_key].mandatory;
                            lookup[key].object = this.data.meta[activeClass].f[f_key].object;
                            lookup[key].data_id = this.data.meta[activeClass].f[f_key].data_id;
                        }
                        else if(this.data.meta[activeClass].a[a_key] != null) // then asset
                        {
                            lookup[key].shown = this.data.meta[activeClass].a[a_key].shown;
                            lookup[key].routed = this.data.meta[activeClass].a[a_key].routed;
                            lookup[key].mandatory = this.data.meta[activeClass].a[a_key].mandatory;
                            lookup[key].object = this.data.meta[activeClass].a[a_key].object;
                            lookup[key].data_id = this.data.meta[activeClass].a[a_key].data_id;
                        }
                    }
                }

            }

            let data = {};
            for(let key in lookup)
            {
                let id = key;
                if(lookup[key].data_id != null)
                {
                    id = lookup[key].data_id;
                }

                if(data[id] == null)
                {
                    data[id] = {
                        shown: lookup[key].shown,
                        routed: lookup[key].routed,
                        mandatory: lookup[key].mandatory,
                        object: lookup[key].object,
                        data_id: lookup[key].data_id,
                        hierarchy: [],
                        works: {},
                    }
                }

                for(let w in lookup[key].works)
                {
                    data[id].works[w] = true;
                }

                data[id].hierarchy.push({
                    process: lookup[key].process,
                    function: lookup[key].function,
                    asset: lookup[key].asset,
                })
            }

            
            let sdata = [];
            for(let line in data)
            {
                if(data[line].shown == 'Yes')
                {
                    sdata.push(data[line]);
                }
            }
            sdata.sort(function(a,b) { return (a.hierarchy[0].asset < b.hierarchy[0].asset) ? -1 : (a.hierarchy[0].asset > b.hierarchy[0].asset) ? 1 : 0 });
            sdata.sort(function(a,b) { return (a.hierarchy[0].function < b.hierarchy[0].function) ? -1 : (a.hierarchy[0].function > b.hierarchy[0].function) ? 1 : 0 });
            sdata.sort(function(a,b) { return (a.hierarchy[0].process < b.hierarchy[0].process) ? -1 : (a.hierarchy[0].process > b.hierarchy[0].process) ? 1 : 0 });

            return sdata;

        },


        
    },


}