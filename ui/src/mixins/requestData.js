
import axios from 'axios';

export default {

    components: {
    },
    data () {
        return {
            data: {
                works: {},
                hierarchy: {},
                meta: {},
                estate: {},
            }
        };
    },
    watch: {
    },
    computed: {
        classList () {
            let list = {};
            for(let key in this.data.works)
            {   
                list[this.data.works[key].site_class] = true;
            }
            return list;
        },
    },
    methods: {


        resetData() {
            this.getEstate();
        },
        
        getEstate () {
            axios.get('http://localhost:3001/estate')
                .then(this.processEstate)
                .catch((e) => console.log(e))
        },
        processEstate (data) {  
            let store = {};

            for(let i=0; i<data.data.length; i++)
            {
                store[data.data[i].type] = {
                    total: data.data[i].total,
                    works: {},
                }
            }
            this.data.estate = store;
            this.getWorks();
        },

        getWorks () {
            axios.get('http://localhost:3001/works').then(this.processWorks).catch((e) => console.log(e))
        },
        processWorks (data) {  
            let works = {};
            for(let i=0; i<data.data.length; i++)
            {
                works[data.data[i].name] = data.data[i];
            }
            this.data.works = works;
            this.getHierarchy();
        },

        getHierarchy () {
            axios.get('http://localhost:3001/hierarchy').then(this.processHierarchy).catch((e) => console.log(e))
        },
        processHierarchy (data) {                
            this.data.hierarchy = data.data;
            this.getMeta();
        },
        
        getMeta () {
            axios.get('http://localhost:3001/meta').then(this.processMeta).catch((e) => console.log(e))
        },
        processMeta (data) {
            let meta = {};
            for(let i=0; i<data.data.length; i++)
            {
                let row = data.data[i];
                if(meta[row.site_class] == null)
                {
                    meta[row.site_class] = {
                        p: {},
                        f: {},
                        a: {},
                    };
                }

                let t = 'a';
                let key = `${row.process} ${row.function} ${row.asset}`;
                if(row.asset == null && row.function == null)
                { // process
                    t = 'p';
                    key = `${row.process}`;
                }
                else if(row.asset == null)
                { // function
                    t = 'f';
                    key = `${row.process} ${row.function}`;
                }
                meta[row.site_class][t][key] = data.data[i];
            }
            this.data.meta = meta;
        },
        
    },


}